UncerTiSe - toolbox to analyse time series with uncertainties
=============================================================


General Notes
-------------

UncerTiSe is a Python package that contains an array of modules to help analyse
time series with uncertainties. This is part of the DFG Project "**Impacts of
uncertainties in climate data analyses (IUCliD): Approaches to working with
measurements as a series of probability distributions**" (DFG MA4759/8).

Installation
------------

As of now, there is not setup.py to install this package. To use it, just clone
the contents of this Git repository in your working directory.

Requirements
------------

The Python port needs the following Python packages:

Scipy with Scipy.Weave
Numpy
Matplotlib (optional) 
Python Igraph


Usage
-----

Note: The modules in this package are inter-dependent! They will not function
properly if they are moved/copied to a separate directory all on their own.

Example
-------

TBD

References
----------

B. Goswami, N. Boers, A. Rheinwalt, N. Marwan, J. Heitzig, S. F. M.
Breitenbach, J. Kurths
Abrupt transitions in time series with uncertainties, 
Nature Communications 9 (2018) 
doi:10.1038/s41467-017-02456-6


Author
------

Bedartha Goswami 

License
-------

This work is licensed under a Creative Commons
Attribution-NonCommercial-NoDerivatives 4.0 International Public License. For
details please read LICENSE.txt.

Please respect the copyrights! The content is protected by the Creative Commons
License. If you use the provided programmes, text or figures, you have to refer
to the given publications and this web site (tocsy.pik-potsdam.de) as well. 
