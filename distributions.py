"""
A few functions to assist in estimating the cumulative distributions
====================================================================

!! Please import as a module and use it as such -- not as a script !!

"""
# Created: Thu May 26, 2016  11:12PM
# Last modified: Mon Apr 15, 2019  09:22pm
# Copyright: Bedartha Goswami <goswami@pik-potsdam.de>


import numpy as np

from utils import _printmsg
from utils import _progressbar_start
from utils import _progressbar_update
from utils import _progressbar_finish


def cumulative(pdfmat, var_span, verbose, pbar):
    """
    Estimates the cumulative distributions for the specified dataset.
    """
    # get CDF matrix and Inter-Quartile Range
    cdfmat = cdf_matrix(pdfmat, var_span, verbose, pbar)
    iqr = inter_quartile_range(cdfmat, var_span, verbose)

    return cdfmat, iqr


def cdf_matrix(pdfmat, var_span, verbose=False, pbar=False):
    """
    Returns Cumulative Distribution Functions from given PDFs.
    """
    nt = pdfmat.shape[0]
    bj = 0.5 * np.r_[
        var_span[1] - var_span[0],
        var_span[2:] - var_span[:-2],
        var_span[-1] - var_span[-2]
    ]  # Riemann sum width
    _printmsg("Estimating CDFs...", verbose)
    cdfmat = np.zeros(pdfmat.shape)
    prog_bar = _progressbar_start(nt, pbar)
    for i in range(nt):
        pdf = pdfmat[i]
        cdfmat[i] = np.cumsum(pdf * bj)
        _progressbar_update(prog_bar, i + 1)
    _progressbar_finish(prog_bar)
    return cdfmat


def inter_quartile_range(cdfmat, var_span, verbose=False):
    """
    Returns the Inter-Quartile Range for the paleo dataset.
    """
    nt = cdfmat.shape[0]
    _printmsg("Estimating IQR of total proxy probability...", verbose)
    cdfhist = np.zeros(len(var_span))
    for i in range(nt):
        cdfhist += cdfmat[i]
    cdfhist /= float(nt)
    qHi = np.interp(0.75, cdfhist, var_span)
    qLo = np.interp(0.25, cdfhist, var_span)
    iqr = qHi - qLo
    return iqr
