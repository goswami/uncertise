__all__ = [
           "utils",
           "distributions",
           "networks",
           "events",
           ]

# define the number of surrogates for the random model (configuration model)
NSURR = 1000

# significance levels
ALPHA_TRANSITION = 0.05
